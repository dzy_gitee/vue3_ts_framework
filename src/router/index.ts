import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import MainComponent from '../view/page/router'
const routes: RouteRecordRaw[] = [
  {
    path: '/',
    name: '_home',
    redirect: {
      name: 'login'
    }
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('@/view/login/login.vue') // 注意这里要带上 文件后缀.vue
  },
  {
    path: '/logout',
    name: 'logout',
    component: () => import('@/view/logout/index.vue')
  },
  {
    path: '/main',
    name: 'main',
    component: () => import('@/view/layout/layout.vue'),
    redirect: {
      name: 'home'
    },
    children: MainComponent
  },
  // error页面
  {
    path: '/401',
    name: 'error_401',
    component: () => import('@/view/error-page/401.vue')
  },
  {
    path: '/500',
    name: 'error_500',
    component: () => import('@/view/error-page/500.vue')
  },
  {
    path: '/:pathMatch(.*)',
    name: 'error_404',
    component: () => import('@/view/error-page/404.vue')
  }
]
const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
