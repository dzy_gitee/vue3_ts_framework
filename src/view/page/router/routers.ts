import Parent from '../layout/index.vue'
import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
const routes: RouteRecordRaw[] = [
  {
    path: '/home',
    name: 'home',
    component: () => import('../home/index.vue')
  }
]

export default routes
